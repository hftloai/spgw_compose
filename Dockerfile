################## BEGIN INSTALLATION ######################
FROM ubuntu:16.04

# Install general dependencies
RUN apt update
RUN apt install -y \
	wget \
	apt-utils \
	sudo \
	psmisc
# Install dependecies
RUN apt install -y \
	autoconf \
	automake \
	bison \
	build-essential \
	cmake \
	cmake-curses-gui \
	doxygen \
	doxygen-gui \
	flex \
	gccxml \
	gdb \
	git \
	pkg-config \
	subversion
# Install libraries
RUN apt install -y \
	guile-2.0-dev \
	libconfig8-dev \
	libgcrypt11-dev \
	libgcrypt-dev \
	libgmp-dev \
	libhogweed? \
	libgtk-3-dev \
	libidn2-0-dev \
	libidn11-dev \
	libpthread-stubs0-dev \
	libtool \
	libxml2 \
	libxml2-dev \
	mscgen \
	openssl \
	python
# Install network tools
RUN apt install -y \
	ethtool \
	iproute \
	vlan \
	iptables
#
RUN apt install -y \
	check \
	python-dev \
	python-pexpect \
	unzip

# install_libgtpnl_from_source
RUN apt install -y \
	libmnl-dev

WORKDIR /tmp
RUN rm -rf /tmp/libgtpnl*
RUN git clone git://git.osmocom.org/libgtpnl
WORKDIR /tmp/libgtpnl
RUN autoreconf -fi
RUN ./configure
RUN make -j`nproc`
RUN make install
RUN ldconfig
WORKDIR /tmp
RUN rm -rf /tmp/libgtpnl*
WORKDIR /

#Install FreeDiameter
RUN apt install -y \
	debhelper \
	g++ \
	gcc \
	libgcrypt-dev \
	libgnutls-dev \
	libpq-dev \
	libxml2-dev \
	mercurial \
	python-dev \
	ssl-cert \
      	swig \
	libsctp-dev
WORKDIR /tmp
RUN echo "Downloading 1.2.0 freeDiameter archive"
RUN rm -rf 1.2.0.tar.gz* freeDiameter-1.2.0 freediameter
RUN GIT_SSL_NO_VERIFY=true git clone https://gitlab.eurecom.fr/oai/freediameter.git -b eurecom-1.2.0
RUN mkdir freediameter/build
WORKDIR /tmp/freediameter/build
RUN cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr/local ../
RUN echo "Compiling freeDiameter"
RUN make -j`nproc`
RUN make install
RUN rm -rf /tmp/1.2.0.tar.gz /tmp/freeDiameter-1.2.0 /tmp/freediameter
WORKDIR /

# install_asn1c_from_source
RUN rm -rf /tmp/asn1c
RUN mkdir -p /tmp/asn1c
RUN git clone https://gitlab.eurecom.fr/oai/asn1c.git /tmp/asn1c
WORKDIR /tmp/asn1c
RUN ./configure
RUN make install
WORKDIR /

# install kmod for modprobe
RUN apt install -y kmod

################## END INSTALLATION ######################

################## INSTALL wait-for script ################
RUN git clone https://github.com/Eficode/wait-for.git
################## END INSTALL wait-for script ################

################## BEGIN CONFIGURING OAI ######################
# Create configuration folder
RUN rm -rf /usr/local/etc/oai
RUN mkdir /usr/local/etc/oai
RUN mkdir /usr/local/etc/oai/freeDiameter

# Copy OpenAir Interface configuration files
COPY oai_conf/spgw.conf /usr/local/etc/oai/
COPY oai_conf/freeDiameter /usr/local/etc/oai/freeDiameter

# Clone OpenAir Interface
RUN git clone -b develop https://gitlab.eurecom.fr/oai/openair-cn.git

# Build HSS
WORKDIR /openair-cn/scripts
RUN ./build_spgw --clean

CMD ./run_spgw
################## END CONFIGURING OAI ######################
